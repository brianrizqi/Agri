package com.example.brianr.agri;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.brianr.agri.Adapter.PetaniProductAdapter;
import com.example.brianr.agri.Model.m_product_petani;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Penjual3Fragment extends Fragment {
    private ListView listView;
    private List<m_product_petani> list;
    private PetaniProductAdapter adapter;
    String id;
    private String get_product_idUrl = BaseAPI.get_product_idURL;

    public Penjual3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_penjual3, container, false);
        id = getArguments().getString("id");
        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
        listView = (ListView) view.findViewById(R.id.listProductPetani);
        list = new ArrayList<>();
        adapter = new PetaniProductAdapter(getActivity(), list);
        listView.setAdapter(adapter);

        getProduct();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                m_product_petani m = list.get(position);
                Intent i = new Intent(getActivity(),PetaniEditProduct.class);
                i.putExtra("id_product",m.getId());
                i.putExtra("nama_tanaman",m.getJudul());
                i.putExtra("harga",m.getHarga());
                i.putExtra("stok",m.getStok());
                i.putExtra("gambar",m.getImg());
                startActivity(i);
            }
        });

        return view;
    }

    private void getProduct() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, get_product_idUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final m_product_petani m = new m_product_petani();
                                m.setId(object.getString("id_product"));
                                m.setJudul(object.getString("nama_tanaman"));
                                m.setHarga(object.getString("harga"));
                                m.setStok(object.getString("stok"));
                                m.setImg(BaseAPI.imageURL + object.getString("gambar"));
                                int veriff = Integer.parseInt(object.getString("verif"));
                                if (veriff == 1) {
                                    m.setVerif("verifikasi");
                                } else {
                                    m.setVerif("Belum verif");
                                }
                                list.add(m);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

}
