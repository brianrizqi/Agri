package com.example.brianr.agri;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class PetaniPromoTanam extends AppCompatActivity {
    private TextView sayur;
    private EditText txtHarga, txtStok;
    private Button kirim;
    private String harga, stok, id, nama_sayur, gambar;
    private ImageView upload;
    private CustomNetworkImageView imgUpload;
    private String verif = "0";
    private Bitmap bitmap;
    private String tambahUrl = BaseAPI.tambah_productURL;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private NetworkImageView img;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petani_promo_tanam);
        sayur = (TextView) findViewById(R.id.namaSayur);
        txtHarga = (EditText) findViewById(R.id.txtHarga);
        txtStok = (EditText) findViewById(R.id.txtStok);
        upload = (ImageView) findViewById(R.id.imageUpload);
        imgUpload = (CustomNetworkImageView) findViewById(R.id.imageProduct);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(i, 1);
            }
        });
        id = getIntent().getStringExtra("id_tanam");
        nama_sayur = getIntent().getStringExtra("nama_tanaman");

        sayur.setText(nama_sayur);
        kirim = (Button) findViewById(R.id.kirimProduct);
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kirimProduct();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                imgUpload.setLocalImageBitmap(bitmap);
                img.setImageUrl(gambar,imageLoader);
//                upload.setImageBitmap(bitmap);
                upload.setBackground(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void kirimProduct() {
        harga = txtHarga.getText().toString().trim();
        stok = txtStok.getText().toString().trim();
        gambar = imageToString(bitmap);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, tambahUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse( String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status){
                                Toast.makeText(PetaniPromoTanam.this, "Success", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else{
                                Toast.makeText(PetaniPromoTanam.this, "Error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(PetaniPromoTanam.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PetaniPromoTanam.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map <String,String> map = new HashMap<String, String>();
                map.put("id_tanam",id);
                map.put("harga",harga);
                map.put("stok",stok);
                map.put("gambar",gambar);
                map.put("verif",verif);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
