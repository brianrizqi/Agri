package com.example.brianr.agri;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RelativeLayout;

public class Petani extends AppCompatActivity {
    RelativeLayout fragment;
    android.support.v4.app.Fragment home, profile, product,tanam;
    String id, nama, email, alamat;

    private BottomNavigationView.OnNavigationItemSelectedListener listener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Bundle data = new Bundle();
            android.support.v4.app.Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = home;
                    if (fragment == null) {
                        home = new Penjual1Fragment();
                    }
                    break;
                case R.id.navigation_tanam:
                    data.putString("id", id);
                    fragment = tanam;
                    if (fragment == null) {
                        tanam = new Penjual2Fragment();
                        fragment = tanam;
                        tanam.setArguments(data);
                    }
                    break;
                case R.id.navigation_product:
                    data.putString("id", id);
                    fragment = product;
                    if (fragment == null) {
                        product = new Penjual3Fragment();
                        fragment = product;
                        product.setArguments(data);
                    }
                    break;
                case R.id.navigation_profile:
                    data.putString("id", id);
                    fragment = profile;
                    if (fragment == null) {
                        profile = new Penjual4Fragment();
                        fragment = profile;
                        profile.setArguments(data);
                    }
                    break;
            }
            if (fragment != null)
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commit();
            return fragment != null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petani);
        fragment = (RelativeLayout) findViewById(R.id.fragment);

        Penjual1Fragment penjual1Fragment = new Penjual1Fragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, penjual1Fragment);
        fragmentTransaction.commit();

        id = getIntent().getStringExtra("id");
        nama = getIntent().getStringExtra("nama");
        email = getIntent().getStringExtra("email");
        alamat = getIntent().getStringExtra("alamat");

        BottomNavigationView navigationView = (BottomNavigationView) findViewById(R.id.bottomNav);
        navigationView.setOnNavigationItemSelectedListener(listener);
        BottomNavigationHelper.disableShiftMode(navigationView);
    }
}
