package com.example.brianr.agri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PembeliDetailProduct extends AppCompatActivity {
    private TextView sayur,txtHarga,txtStok;
    private EditText txtJumlah;
    private Button beli,batal;
    private String harga, stok, id, nama_sayur, gambar,jumlah,idUser,verif;
    private CustomNetworkImageView imgUpload;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private String beli_productUrl = BaseAPI.beli_productURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembeli_detail_product);
        sayur = (TextView) findViewById(R.id.namaSayur);
        txtHarga = (TextView) findViewById(R.id.txtHarga);
        txtStok = (TextView)findViewById(R.id.txtStok);
        txtJumlah = (EditText) findViewById(R.id.txtJumlah);
        imgUpload = (CustomNetworkImageView)findViewById(R.id.imageProduct);
        beli = (Button) findViewById(R.id.beliProduct);
        batal = (Button) findViewById(R.id.batalProduct);

        id = getIntent().getStringExtra("id_product");
        harga = getIntent().getStringExtra("harga");
        nama_sayur = getIntent().getStringExtra("nama_tanaman");
        stok = getIntent().getStringExtra("stok");
        gambar = getIntent().getStringExtra("gambar");
        idUser = getIntent().getStringExtra("id");

        sayur.setText(nama_sayur);
        txtHarga.setText(harga);
        txtStok.setText(stok);
        imgUpload.setImageUrl(gambar,imageLoader);

        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beliProduct();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void beliProduct(){
        verif = "0";
        jumlah = txtJumlah.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, beli_productUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status){
                                Toast.makeText(PembeliDetailProduct.this, "success", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(PembeliDetailProduct.this, "error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(PembeliDetailProduct.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PembeliDetailProduct.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map <String,String> map = new HashMap<String, String>();
                map.put("id_product",id);
                map.put("id",idUser);
                map.put("jumlah",jumlah);
                map.put("verif_trans",verif);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
