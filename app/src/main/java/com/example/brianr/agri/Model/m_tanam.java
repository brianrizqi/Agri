package com.example.brianr.agri.Model;

import java.util.ArrayList;

public class m_tanam {
    private String id, judul, tanggal, tempat;

    public m_tanam() {
    }

    public m_tanam(String id, String judul, String tanggal, String tempat) {
        this.id = id;
        this.judul = judul;
        this.tanggal = tanggal;
        this.tempat = tempat;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
