package com.example.brianr.agri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PenjualVerifPenjualan extends AppCompatActivity {
    private Button verifBtn;
    private TextView txtSayur, txtHarga, txtPembeli, txtJumlah;
    private String id_transaksi, harga, nama, jumlah, nama_tanaman,gambar;
    private String verif = "1";
    private String verif_productUrl = BaseAPI.verif_transaksiURL;
    private CustomNetworkImageView image_product;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    Locale locale = new Locale("in", "ID");
    NumberFormat format = NumberFormat.getCurrencyInstance(locale);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penjual_verif_penjualan);
        txtHarga = (TextView) findViewById(R.id.txtHarga);
        txtPembeli = (TextView) findViewById(R.id.txtPembeli);
        txtSayur = (TextView) findViewById(R.id.namaSayur);
        txtJumlah = (TextView) findViewById(R.id.txtJumlah);
        image_product = (CustomNetworkImageView) findViewById(R.id.imageProduct);
        verifBtn = (Button) findViewById(R.id.verifProduct);

        id_transaksi = getIntent().getStringExtra("id_transaksi");
        nama = getIntent().getStringExtra("nama");
        nama_tanaman = getIntent().getStringExtra("nama_tanaman");
        harga = getIntent().getStringExtra("harga");
        jumlah = getIntent().getStringExtra("jumlah");
        gambar = getIntent().getStringExtra("gambar");

        txtJumlah.setText("Jumlah : "+jumlah);
        txtSayur.setText(nama_tanaman);
        txtPembeli.setText("Pembeli : "+nama);
        txtHarga.setText("Harga : "+format.format(Double.parseDouble(harga)));
        image_product.setImageUrl(gambar,imageLoader);
        Toast.makeText(this, id_transaksi, Toast.LENGTH_SHORT).show();

        verifBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifTransaksi();
            }
        });
    }

    private void verifTransaksi(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, verif_productUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                Toast.makeText(PenjualVerifPenjualan.this, "Data berhasi di verifikasi", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(PenjualVerifPenjualan.this, "error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PenjualVerifPenjualan.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PenjualVerifPenjualan.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id_transaksi", id_transaksi);
                map.put("verif_trans", verif);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
