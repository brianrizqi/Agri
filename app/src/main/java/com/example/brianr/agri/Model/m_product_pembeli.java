package com.example.brianr.agri.Model;

public class m_product_pembeli {
    private String id, judul, harga, stok, penjual, img;

    public m_product_pembeli(){

    }

    public m_product_pembeli(String id, String judul, String harga, String stok, String penjual, String img) {
        this.id = id;
        this.judul = judul;
        this.harga = harga;
        this.stok = stok;
        this.penjual = penjual;
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getPenjual() {
        return penjual;
    }

    public void setPenjual(String penjual) {
        this.penjual = penjual;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
