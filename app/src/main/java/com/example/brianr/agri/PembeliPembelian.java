package com.example.brianr.agri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.brianr.agri.Adapter.PembeliPembelianAdapter;
import com.example.brianr.agri.Model.m_pembelian_pembeli;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PembeliPembelian extends AppCompatActivity {
    private ListView listView;
    private List<m_pembelian_pembeli> list;
    private PembeliPembelianAdapter adapter;
    private String get_transaksi_idUrl = BaseAPI.get_transaksi_pembeliURL;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembeli_pembelian);
        listView = (ListView) findViewById(R.id.listPembeliPembelian);
        list = new ArrayList<>();
        adapter = new PembeliPembelianAdapter(this, list);
        listView.setAdapter(adapter);
        id = getIntent().getStringExtra("id");
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
        getTransaksi();
    }
    private void getTransaksi() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, get_transaksi_idUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final m_pembelian_pembeli m = new m_pembelian_pembeli();
                                m.setId(object.getString("id_transaksi"));
                                m.setJudul(object.getString("nama_tanaman"));
                                m.setHarga(object.getString("harga"));
                                m.setJumlah(object.getString("jumlah"));
                                m.setNama("");
                                m.setGambar(BaseAPI.imageURL + object.getString("gambar"));
                                int veriff = Integer.parseInt(object.getString("verif_trans"));
                                if (veriff == 1) {
                                    m.setVerif("verifikasi");
                                } else {
                                    m.setVerif("Belum verif");
                                }
                                list.add(m);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PembeliPembelian.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PembeliPembelian.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
