package com.example.brianr.agri.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.brianr.agri.AppController;
import com.example.brianr.agri.Model.m_product_pembeli;
import com.example.brianr.agri.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class PembeliProductAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<m_product_pembeli> list;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public PembeliProductAdapter(Activity activity, List<m_product_pembeli> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(activity, R.layout.list_product_pembeli,null);
        Locale locale = new Locale("in","ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        m_product_pembeli m = list.get(position);

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_product_petani, null);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        NetworkImageView imgProduct = (NetworkImageView)view.findViewById(R.id.imgProduct);
        TextView txtJudul = (TextView) view.findViewById(R.id.txtJudul);
        TextView txtHarga = (TextView) view.findViewById(R.id.txtHarga);
        TextView txtStok = (TextView) view.findViewById(R.id.txtStok);
        TextView txtPenjual = (TextView) view.findViewById(R.id.txtPenjual);

        txtJudul.setText(m.getJudul());
        imgProduct.setImageUrl(m.getImg(),imageLoader);
        txtHarga.setText("Harga : "+format.format(Double.parseDouble(m.getHarga())));
        txtStok.setText("Jumlah Stok : "+m.getStok());
        txtPenjual.setText("Penjual : "+m.getPenjual());
        view.setTag(m.getId());
        return view;
    }
}
