package com.example.brianr.agri;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Pembeli3Fragment extends Fragment {
    private TextView txtNama, txtEmail, txtAlamat;
    private String url = BaseAPI.pembeliProfile;
    private String nama, email, alamat,id;
    private Button pembelian,logout;

    public Pembeli3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pembeli3, container, false);
        txtNama = (TextView) view.findViewById(R.id.profileNama);
        txtEmail = (TextView) view.findViewById(R.id.profileEmail);
        txtAlamat = (TextView) view.findViewById(R.id.profileAlamat);
        pembelian = (Button) view.findViewById(R.id.pembelian);
        logout = (Button) view.findViewById(R.id.logout);
        id = getArguments().getString("id");
        nama = getArguments().getString("nama");
        email = getArguments().getString("email");
        alamat = getArguments().getString("alamat");
        txtNama.setText(nama);
        txtEmail.setText(email);
        txtAlamat.setText(alamat);
        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();

        pembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),PembeliPembelian.class);
                i.putExtra("id",id);
                startActivity(i);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),Login.class);
                startActivity(i);
                getActivity().finish();
            }
        });
        return view;
    }

}
