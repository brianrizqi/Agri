package com.example.brianr.agri;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.brianr.agri.Adapter.AdminProductAdapter;
import com.example.brianr.agri.Model.m_product_admin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Admin2Fragment extends Fragment {
    private ListView listView;
    private AdminProductAdapter adapter;
    private List<m_product_admin> list;
    private String get_product_adminUrl = BaseAPI.get_product_adminURL;

    public Admin2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin2, container, false);
        listView = (ListView) view.findViewById(R.id.listProductAdmin);
        list = new ArrayList<>();
        adapter = new AdminProductAdapter(getActivity(), list);
        listView.setAdapter(adapter);

        getProduct();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                m_product_admin m = list.get(position);
                Intent i = new Intent(getActivity(),AdminVerifProduct.class);
                i.putExtra("id_product",m.getId());
                i.putExtra("nama_tanaman",m.getJudul());
                i.putExtra("harga",m.getHarga());
                i.putExtra("stok",m.getStok());
                i.putExtra("gambar",m.getImg());
                i.putExtra("verif",m.getVerif());
                i.putExtra("nama",m.getPenjual());
                startActivity(i);
            }
        });
        return view;
    }

    private void getProduct(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, get_product_adminUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final m_product_admin m = new m_product_admin();
                                m.setId(object.getString("id_product"));
                                m.setJudul(object.getString("nama_tanaman"));
                                m.setHarga(object.getString("harga"));
                                m.setStok(object.getString("stok"));
                                m.setPenjual(object.getString("nama"));
                                m.setImg(BaseAPI.imageURL + object.getString("gambar"));
                                int veriff = Integer.parseInt(object.getString("verif"));
                                if (veriff == 1) {
                                    m.setVerif("verifikasi");
                                } else {
                                    m.setVerif("Belum verif");
                                }
                                list.add(m);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

}
