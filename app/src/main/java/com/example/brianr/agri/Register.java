package com.example.brianr.agri;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {
    private EditText txtNama, txtEmail, txtNo_ktp, txtAlamat, txtUsername, txtPassword;
    private String nama, email, no_ktp, alamat, username, password;
    private String level = "3";
    private Button regis;
    private String regisUrl = BaseAPI.registerURL;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        txtNama = (EditText) findViewById(R.id.nama);
        txtEmail = (EditText) findViewById(R.id.email);
        txtNo_ktp = (EditText) findViewById(R.id.no_ktp);
        txtAlamat = (EditText) findViewById(R.id.alamat);
        txtUsername = (EditText) findViewById(R.id.username);
        txtPassword = (EditText) findViewById(R.id.password);
        regis = (Button) findViewById(R.id.btnRegis);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userRegis();
            }
        });
    }

    private void userRegis() {
        nama = txtNama.getText().toString().trim();
        email = txtEmail.getText().toString().trim();
        no_ktp = txtNo_ktp.getText().toString().trim();
        alamat = txtAlamat.getText().toString().trim();
        username = txtUsername.getText().toString().trim();
        password = txtPassword.getText().toString().trim();


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Tunggu :)");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, regisUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                Toast.makeText(Register.this, "Register Success", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(Register.this,Login.class);
                                startActivity(i);
                                finish();
                            } else {
                                String errorMsg = jsonObject.getString("error_msg");
                                Toast.makeText(Register.this, errorMsg, Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(Register.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Register.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("nama",nama);
                map.put("email",email);
                map.put("no_ktp",no_ktp);
                map.put("alamat",alamat);
                map.put("username",username);
                map.put("password",password);
                map.put("level",level);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideDialog();
    }

    private void hideDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
}
