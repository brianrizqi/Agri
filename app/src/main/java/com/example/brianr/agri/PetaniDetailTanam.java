package com.example.brianr.agri;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PetaniDetailTanam extends AppCompatActivity {
    private Button edit, hapus, btnTanggal,promo;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TextView txtTanggal, txtSpinner;
    private EditText txtSayur;
    private Spinner spinner;
    String sayur, tanggal, tempat, id;
    private String ubahUrl = BaseAPI.ubah_tanamURL;
    private String hapusUrl = BaseAPI.hapus_tanamURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petani_detail_tanam);
        txtSayur = (EditText) findViewById(R.id.namaSayur);
        btnTanggal = (Button) findViewById(R.id.btnTanggal);
        txtTanggal = (TextView) findViewById(R.id.txtTanggal);
        txtSpinner = (TextView) findViewById(R.id.txtSpinner);

        id = getIntent().getStringExtra("id_tanam");
        sayur = getIntent().getStringExtra("nama_tanaman");
        tanggal = getIntent().getStringExtra("tanggal_tanam");
        tempat = getIntent().getStringExtra("tempat");

        txtSayur.setText(sayur);
        txtTanggal.setText(tanggal);
//        txtSpinner.setText(tempat);

        btnTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PetaniDetailTanam.this,
                        android.R.style.Theme_Holo_Dialog_MinWidth,
                        dateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                txtTanggal.setText(format.format(calendar.getTime()));
            }
        };
        spinner = (Spinner) findViewById(R.id.spinnerTanam);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.tempat_tanam, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getItemAtPosition(position).toString();
                tempat = text;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edit = (Button) findViewById(R.id.editTanam);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTanam();
            }
        });

        hapus = (Button) findViewById(R.id.deleteTanam);
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusTanam();
            }
        });
        promo = (Button) findViewById(R.id.promoTanam);
        promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PetaniDetailTanam.this,PetaniPromoTanam.class);
                i.putExtra("id_tanam",id);
                i.putExtra("nama_tanaman",sayur);
                startActivity(i);
                finish();
            }
        });
    }

    private void editTanam() {
        sayur = txtSayur.getText().toString().trim();
        tanggal = txtTanggal.getText().toString().trim();
//        tempat = txtSpinner.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ubahUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                Toast.makeText(PetaniDetailTanam.this, "Success", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(PetaniDetailTanam.this, "Error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PetaniDetailTanam.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id_tanam", id);
                map.put("nama_tanaman", sayur);
                map.put("tanggal_tanam", tanggal);
                map.put("tempat", tempat);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hapusTanam() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, hapusUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status){
                                Toast.makeText(PetaniDetailTanam.this, "Data Berhasil dihapus", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(PetaniDetailTanam.this, "error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(PetaniDetailTanam.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PetaniDetailTanam.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id_tanam", id);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
