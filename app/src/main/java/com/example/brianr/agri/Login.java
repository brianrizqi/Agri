package com.example.brianr.agri;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    private EditText txtUsername, txtPassword;
    private String username, password, email, IDuser, alamat, nama;
    private Button login, regis;
    private String loginUrl = BaseAPI.loginURL;
    int level;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtUsername = (EditText) findViewById(R.id.userLogin);
        txtPassword = (EditText) findViewById(R.id.passLogin);
        regis = (Button) findViewById(R.id.btnRegis);
        login = (Button) findViewById(R.id.btnLogin);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin();
            }
        });
    }

    private void userLogin() {
        username = txtUsername.getText().toString().trim();
        password = txtPassword.getText().toString().trim();
        if (username.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            Toast.makeText(this, "Username / Password kosong", Toast.LENGTH_SHORT).show();
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Tunggu :)");
            pDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, loginUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean error = jsonObject.getBoolean("error");
                                if (!error) {
                                    IDuser = jsonObject.getString("id");
                                    nama = jsonObject.getString("nama");
                                    email = jsonObject.getString("email");
                                    alamat = jsonObject.getString("alamat");
                                    level = jsonObject.getInt("level");


                                    if (level == 1) {
                                        Intent i = new Intent(Login.this, AdminActivity.class);
                                        i.putExtra("id", IDuser);
                                        i.putExtra("nama", nama);
                                        i.putExtra("email", email);
                                        i.putExtra("alamat", alamat);
                                        i.putExtra("level", level);
                                        startActivity(i);
                                        finish();
                                    } else if (level == 2) {
                                        Intent i = new Intent(Login.this, Petani.class);
                                        i.putExtra("id", IDuser);
                                        i.putExtra("nama", nama);
                                        i.putExtra("email", email);
                                        i.putExtra("alamat", alamat);
                                        i.putExtra("level", level);
                                        startActivity(i);
                                        finish();

                                    } else if (level == 3){
                                        Intent i = new Intent(Login.this, Pembeli.class);
                                        i.putExtra("id", IDuser);
                                        i.putExtra("nama", nama);
                                        i.putExtra("email", email);
                                        i.putExtra("alamat", alamat);
                                        i.putExtra("level", level);
                                        startActivity(i);
                                        finish();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(Login.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("username", username);
                    map.put("password", password);
                    return map;
                }
            };
            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideDialog();
    }

    private void hideDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
}
