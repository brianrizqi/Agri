package com.example.brianr.agri.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.brianr.agri.AppController;
import com.example.brianr.agri.Model.m_penjualan_petani;
import com.example.brianr.agri.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class PetaniPenjualanAdapter extends BaseAdapter {
    private Context context;
    private List<m_penjualan_petani> list;
    private LayoutInflater inflater;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public PetaniPenjualanAdapter(Context context, List<m_penjualan_petani> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.list_penjualan_petani,null);
        m_penjualan_petani m = list.get(position);
        Locale locale = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);

        if (inflater == null)
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_product_petani, null);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        NetworkImageView imgProduct = (NetworkImageView) view.findViewById(R.id.imgProduct);
        TextView txtJudul = (TextView) view.findViewById(R.id.txtJudul);
        TextView txtHarga = (TextView) view.findViewById(R.id.txtHarga);
        TextView txtJumlah = (TextView) view.findViewById(R.id.txtJumlah);
        TextView txtVerif = (TextView) view.findViewById(R.id.txtVerif);
        TextView txtPembeli = (TextView) view.findViewById(R.id.txtPembeli);

        txtJudul.setText(m.getJudul());
        imgProduct.setImageUrl(m.getGambar(), imageLoader);
        txtPembeli.setText("Pembeli : " + m.getNama());
        txtHarga.setText("Harga : " + format.format(Double.parseDouble(m.getHarga())));
        txtJumlah.setText("Jumlah Stok : " + m.getJumlah());
        txtVerif.setText("Status : " + m.getVerif());
        view.setTag(m.getId());
        return view;
    }
}
