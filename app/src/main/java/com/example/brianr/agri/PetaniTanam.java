package com.example.brianr.agri;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PetaniTanam extends AppCompatActivity {
    private EditText txtSayur;
    private Button btnTanggal, simpan;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TextView txtTanggal, txtSpinner;
    private Spinner spinner;
    private String tanamUrl = BaseAPI.tanamURL;
    private String sayur, tanggal, tempat;
    private String IDUser;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petani_tanam);
        txtSayur = (EditText) findViewById(R.id.namaSayur);
        btnTanggal = (Button) findViewById(R.id.btnTanggal);
        txtTanggal = (TextView) findViewById(R.id.txtTanggal);
        txtSpinner = (TextView) findViewById(R.id.txtSpinner);
        IDUser = getIntent().getStringExtra("id");
        Toast.makeText(this, IDUser, Toast.LENGTH_SHORT).show();
        btnTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PetaniTanam.this,
                        android.R.style.Theme_Holo_Dialog_MinWidth,
                        dateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                txtTanggal.setText(format.format(calendar.getTime()));
            }
        };
        spinner = (Spinner) findViewById(R.id.spinnerTanam);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.tempat_tanam, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getItemAtPosition(position).toString();
//                txtSpinner.setText(text);
                tempat = text;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        simpan = (Button) findViewById(R.id.btnSimpanTanam);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpanTanam();
            }
        });
    }

    private void simpanTanam() {
        sayur = txtSayur.getText().toString().trim();
        tanggal = txtTanggal.getText().toString().trim();
//        tempat = txtSpinner.getText().toString().trim();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Tunggu :)");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, tanamUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                Toast.makeText(PetaniTanam.this, "Success", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                                finish();
                            } else {
                                String errorMsg = jsonObject.getString("error_msg");
                                Toast.makeText(PetaniTanam.this, errorMsg, Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PetaniTanam.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            hideDialog();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PetaniTanam.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", IDUser);
                map.put("nama_tanaman", sayur);
                map.put("tanggal_tanam", tanggal);
                map.put("tempat", tempat);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideDialog();
    }

    private void hideDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
}
