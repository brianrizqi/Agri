package com.example.brianr.agri;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RelativeLayout;

public class AdminActivity extends AppCompatActivity {
    RelativeLayout fragment;
    android.support.v4.app.Fragment home,verif,register;
    String id,nama,email,alamat;
    private BottomNavigationView.OnNavigationItemSelectedListener listener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            android.support.v4.app.Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = home;
                    if (fragment == null) {
                        home = new Admin1Fragment();
                    }
                    break;
                case R.id.navigation_verif:
                    fragment = verif;
                    if (fragment == null) {
                        verif = new Admin2Fragment();
                        fragment = verif;
                    }
                    break;
                case R.id.navigation_register:
                    Bundle data = new Bundle();
                    data.putString("id",id);
                    fragment = register;
                    if (fragment == null) {
                        register = new Admin3Fragment();
                        fragment = register;
                        register.setArguments(data);
                    }
                    break;
            }
            if (fragment != null)
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commit();
            return fragment != null;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        fragment = (RelativeLayout) findViewById(R.id.fragment);

        Admin1Fragment admin1Fragment = new Admin1Fragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, admin1Fragment);
        fragmentTransaction.commit();

        id = getIntent().getStringExtra("id");
        nama = getIntent().getStringExtra("nama");
        email = getIntent().getStringExtra("email");
        alamat = getIntent().getStringExtra("alamat");

        BottomNavigationView navigationView = (BottomNavigationView) findViewById(R.id.bottomNav);
        navigationView.setOnNavigationItemSelectedListener(listener);
        BottomNavigationHelper.disableShiftMode(navigationView);
    }
}
