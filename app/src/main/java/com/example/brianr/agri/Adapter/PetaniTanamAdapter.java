package com.example.brianr.agri.Adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.brianr.agri.Model.m_tanam;
import com.example.brianr.agri.R;

import java.util.List;

public class PetaniTanamAdapter extends BaseAdapter {
    private Activity activity;
    private List<m_tanam>list;

    public PetaniTanamAdapter(Activity activity, List<m_tanam> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(activity, R.layout.list_tanam_petani,null);
        m_tanam m = list.get(position);
        TextView judul = (TextView) view.findViewById(R.id.judul);
        TextView tanggal = (TextView) view.findViewById(R.id.tanggal);
        TextView tempat = (TextView)view.findViewById(R.id.tempat);
        judul.setText(m.getJudul());
        tanggal.setText(m.getTanggal());
        tempat.setText(m.getTempat());
        view.setTag(m.getId());
        return view;
    }
}
