package com.example.brianr.agri;

/*
192.168.1.3 wifi rumah
192.168.43.157 hp brian
*/
public class BaseAPI {
    public static final String baseURL = "http://192.168.1.3/agri/";
    public static final String registerURL = baseURL + "register.php";
    public static final String loginURL = baseURL + "login.php";
    public static final String imageURL = baseURL + "gambar/";
    public static final String pembeliProfile = baseURL + "user.php?apicall=get_user";
    public static final String tanamURL = baseURL + "tanam.php?apicall=tambah_tanam";
    public static final String get_tanamURL = baseURL + "tanam.php?apicall=get_tanam_id";
    public static final String ubah_tanamURL = baseURL + "tanam.php?apicall=ubah_tanam";
    public static final String hapus_tanamURL = baseURL + "tanam.php?apicall=hapus_tanam";
    public static final String tambah_productURL = baseURL + "product.php?apicall=tambah_product";
    public static final String ubah_productURL = baseURL + "product.php?apicall=ubah_product";
    public static final String hapus_productURL = baseURL + "product.php?apicall=hapus_product";
    public static final String get_product_idURL = baseURL + "product.php?apicall=get_product_id";
    public static final String get_productURL = baseURL + "product.php?apicall=get_product";
    public static final String get_product_adminURL = baseURL + "product.php?apicall=get_product_admin";
    public static final String verif_productURL = baseURL + "product.php?apicall=verif_product";
    public static final String beli_productURL = baseURL + "transaksi.php?apicall=beli_product";
    public static final String get_transaksi_idURL = baseURL +"transaksi.php?apicall=get_transaksi_id";
    public static final String verif_transaksiURL = baseURL +"transaksi.php?apicall=verif_transaksi";
    public static final String get_transaksi_pembeliURL = baseURL +"transaksi.php?apicall=get_transaksi_pembeli";
}
