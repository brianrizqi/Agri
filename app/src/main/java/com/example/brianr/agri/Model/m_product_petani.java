package com.example.brianr.agri.Model;

public class m_product_petani {
    private String id, judul, harga, stok, verif, img;

    public m_product_petani() {

    }

    public m_product_petani(String id, String judul, String harga, String stok, String verif, String img) {
        this.id = id;
        this.judul = judul;
        this.harga = harga;
        this.stok = stok;
        this.verif = verif;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getVerif() {
        return verif;
    }

    public void setVerif(String verif) {
        this.verif = verif;
    }
}
