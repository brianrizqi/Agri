package com.example.brianr.agri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AdminVerifProduct extends AppCompatActivity {
    private Button verifBtn;
    private TextView txtSayur, txtHarga, txtPenjual, txtStok;
    private String id_product, harga, nama, stok, nama_tanaman,gambar;
    private String verif = "1";
    private String verif_productUrl = BaseAPI.verif_productURL;
    private CustomNetworkImageView image_product;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    Locale locale = new Locale("in", "ID");
    NumberFormat format = NumberFormat.getCurrencyInstance(locale);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_verif_product);
        txtHarga = (TextView) findViewById(R.id.txtHarga);
        txtPenjual = (TextView) findViewById(R.id.txtPenjual);
        txtSayur = (TextView) findViewById(R.id.namaSayur);
        txtStok = (TextView) findViewById(R.id.txtStok);
        image_product = (CustomNetworkImageView) findViewById(R.id.imageProduct);
        verifBtn = (Button) findViewById(R.id.verifProduct);

        id_product = getIntent().getStringExtra("id_product");
        nama = getIntent().getStringExtra("nama");
        nama_tanaman = getIntent().getStringExtra("nama_tanaman");
        harga = getIntent().getStringExtra("harga");
        stok = getIntent().getStringExtra("stok");
        gambar = getIntent().getStringExtra("gambar");

        txtStok.setText(stok);
        txtSayur.setText(nama_tanaman);
        txtPenjual.setText(nama);
        txtHarga.setText(harga);
        image_product.setImageUrl(gambar,imageLoader);

        verifBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifProduct();
            }
        });
    }

    private void verifProduct() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, verif_productUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                Toast.makeText(AdminVerifProduct.this, "Data berhasi di verifikasi", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(AdminVerifProduct.this, "error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(AdminVerifProduct.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(AdminVerifProduct.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id_product", id_product);
                map.put("verif", verif);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
