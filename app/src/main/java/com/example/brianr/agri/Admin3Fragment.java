package com.example.brianr.agri;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Admin3Fragment extends Fragment {
    private EditText txtNama, txtEmail, txtNo_ktp, txtAlamat, txtUsername, txtPassword;
    private String nama, email, no_ktp, alamat, username, password;
    private String level = "2";
    private Button regis;
    private String regisUrl = BaseAPI.registerURL;
    private ProgressDialog pDialog;

    public Admin3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin3, container, false);
        txtNama = (EditText) view.findViewById(R.id.nama);
        txtEmail = (EditText) view.findViewById(R.id.email);
        txtNo_ktp = (EditText) view.findViewById(R.id.no_ktp);
        txtAlamat = (EditText) view.findViewById(R.id.alamat);
        txtUsername = (EditText) view.findViewById(R.id.username);
        txtPassword = (EditText) view.findViewById(R.id.password);
        regis = (Button) view.findViewById(R.id.btnRegis);

        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userRegis();
            }
        });

        return view;
    }

    private void userRegis() {
        nama = txtNama.getText().toString().trim();
        email = txtEmail.getText().toString().trim();
        no_ktp = txtNo_ktp.getText().toString().trim();
        alamat = txtAlamat.getText().toString().trim();
        username = txtUsername.getText().toString().trim();
        password = txtPassword.getText().toString().trim();


        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Tunggu :)");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, regisUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                Toast.makeText(getActivity(), "Register Success", Toast.LENGTH_SHORT).show();
                            } else {
                                String errorMsg = jsonObject.getString("error_msg");
                                Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("nama", nama);
                map.put("email", email);
                map.put("no_ktp", no_ktp);
                map.put("alamat", alamat);
                map.put("username", username);
                map.put("password", password);
                map.put("level", level);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideDialog();
    }

    private void hideDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }


}
