package com.example.brianr.agri;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.brianr.agri.Adapter.PetaniTanamAdapter;
import com.example.brianr.agri.Model.m_tanam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Penjual2Fragment extends Fragment {
    private FloatingActionButton fab;
    private List<m_tanam> list;
    private ListView listView;
    private PetaniTanamAdapter adapter;
    private String getUrl = BaseAPI.get_tanamURL;
    String id;

    public Penjual2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_penjual2, container, false);
        id = getArguments().getString("id");
        listView = (ListView) view.findViewById(R.id.listPetani);
        list = new ArrayList<>();
        adapter = new PetaniTanamAdapter(getActivity(), list);
        listView.setAdapter(adapter);

        getTanam();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                m_tanam m = list.get(position);
                Intent i = new Intent(getActivity(),PetaniDetailTanam.class);
                i.putExtra("id_tanam",m.getId());
                i.putExtra("nama_tanaman",m.getJudul());
                i.putExtra("tanggal_tanam",m.getTanggal());
                i.putExtra("tempat",m.getTempat());
                startActivity(i);
            }
        });

        fab = (FloatingActionButton) view.findViewById(R.id.fabPetani);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PetaniTanam.class);
                i.putExtra("id", id);
                startActivity(i);
            }
        });

        return view;
    }

    private void getTanam(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, getUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final m_tanam m_tanam = new m_tanam();
                                m_tanam.setId(object.getString("id_tanam"));
                                m_tanam.setJudul(object.getString("nama_tanaman"));
                                m_tanam.setTanggal(object.getString("tanggal_tanam"));
                                m_tanam.setTempat(object.getString("tempat"));
                                list.add(m_tanam);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

}
