package com.example.brianr.agri;

import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RelativeLayout;

public class Pembeli extends AppCompatActivity {
    RelativeLayout fragment;
    android.support.v4.app.Fragment home,profile,product;
    String id,nama,email,alamat;

    private BottomNavigationView.OnNavigationItemSelectedListener listener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            android.support.v4.app.Fragment fragment = null;
            Bundle data = new Bundle();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = home;
                    if (fragment == null) {
                        home = new Pembeli1Fragment();
                    }
                    break;
                case R.id.navigation_product:
                    data.putString("id",id);
                    fragment = product;
                    if (fragment == null) {
                        product = new Pembeli2Fragment();
                        fragment = product;
                        product.setArguments(data);
                    }
                    break;
                case R.id.navigation_profile:
                    data.putString("id",id);
                    data.putString("nama",nama);
                    data.putString("email",email);
                    data.putString("alamat",alamat);
                    fragment = profile;
                    if (fragment == null) {
                        profile = new Pembeli3Fragment();
                        fragment = profile;
                        profile.setArguments(data);
                    }
                    break;
            }
            if (fragment != null)
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commit();
            return fragment != null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembeli);
        fragment = (RelativeLayout) findViewById(R.id.fragment);
        Pembeli1Fragment pembeli1Fragment = new Pembeli1Fragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, pembeli1Fragment);
        fragmentTransaction.commit();

        id = getIntent().getStringExtra("id");
        nama = getIntent().getStringExtra("nama");
        email = getIntent().getStringExtra("email");
        alamat = getIntent().getStringExtra("alamat");

        BottomNavigationView navigationView = (BottomNavigationView) findViewById(R.id.bottomNav);
        navigationView.setOnNavigationItemSelectedListener(listener);
        BottomNavigationHelper.disableShiftMode(navigationView);
    }
}
