package com.example.brianr.agri;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PetaniEditProduct extends AppCompatActivity {
    private TextView sayur;
    private EditText txtHarga, txtStok;
    private Button edit,hapus;
    private String harga, stok, id, nama_sayur, gambar;
    private CustomNetworkImageView imgUpload;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private String ubah_productUrl = BaseAPI.ubah_productURL;
    private String hapus_productUrl = BaseAPI.hapus_productURL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petani_edit_product);
        sayur = (TextView) findViewById(R.id.namaSayur);
        txtHarga = (EditText) findViewById(R.id.txtHarga);
        txtStok = (EditText) findViewById(R.id.txtStok);
        imgUpload = (CustomNetworkImageView)findViewById(R.id.imageProduct);

        id = getIntent().getStringExtra("id_product");
        harga = getIntent().getStringExtra("harga");
        nama_sayur = getIntent().getStringExtra("nama_tanaman");
        stok = getIntent().getStringExtra("stok");
        gambar = getIntent().getStringExtra("gambar");

        sayur.setText(nama_sayur);
        txtHarga.setText(harga);
        txtStok.setText(stok);
        imgUpload.setImageUrl(gambar,imageLoader);

        edit = (Button) findViewById(R.id.editProduct);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProduct();
            }
        });
        hapus = (Button) findViewById(R.id.hapusProduct);
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusProduct();
            }
        });
    }
    private void editProduct(){
        harga = txtHarga.getText().toString().trim();
        stok = txtStok.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ubah_productUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status){
                                Toast.makeText(PetaniEditProduct.this, "Data berhasi diubah", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(PetaniEditProduct.this, "Error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(PetaniEditProduct.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PetaniEditProduct.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map <String,String> map = new HashMap<String, String>();
                map.put("id_product",id);
                map.put("harga",harga);
                map.put("stok",stok);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    private void hapusProduct(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, hapus_productUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status){
                                Toast.makeText(PetaniEditProduct.this, "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(PetaniEditProduct.this, "error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(PetaniEditProduct.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PetaniEditProduct.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map <String,String> map = new HashMap<String, String>();
                map.put("id_product",id);
                map.put("gambar",gambar);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
