package com.example.brianr.agri;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.brianr.agri.Adapter.PetaniPenjualanAdapter;
import com.example.brianr.agri.Adapter.PetaniTanamAdapter;
import com.example.brianr.agri.Model.m_penjualan_petani;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PetaniPenjualan extends AppCompatActivity {
    private ListView listView;
    private List<m_penjualan_petani> list;
    private PetaniPenjualanAdapter adapter;
    private String get_transaksi_idUrl = BaseAPI.get_transaksi_idURL;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petani_penjualan);
        listView = (ListView) findViewById(R.id.listPetaniPenjualan);
        list = new ArrayList<>();
        adapter = new PetaniPenjualanAdapter(this, list);
        listView.setAdapter(adapter);
        id = getIntent().getStringExtra("id");
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();

        getTransaksi();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                m_penjualan_petani m = list.get(position);
                Intent i = new Intent(PetaniPenjualan.this, PenjualVerifPenjualan.class);
                i.putExtra("id_transaksi",m.getId());
                i.putExtra("nama_tanaman",m.getJudul());
                i.putExtra("harga",m.getHarga());
                i.putExtra("jumlah",m.getJumlah());
                i.putExtra("gambar",m.getGambar());
                i.putExtra("verif_trans",m.getVerif());
                i.putExtra("nama",m.getNama());
                startActivity(i);
            }
        });
    }

    private void getTransaksi() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, get_transaksi_idUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final m_penjualan_petani m = new m_penjualan_petani();
                                m.setId(object.getString("id_transaksi"));
                                m.setJudul(object.getString("nama_tanaman"));
                                m.setHarga(object.getString("harga"));
                                m.setJumlah(object.getString("jumlah"));
                                m.setNama(object.getString("nama"));
                                m.setGambar(BaseAPI.imageURL + object.getString("gambar"));
                                int veriff = Integer.parseInt(object.getString("verif_trans"));
                                if (veriff == 1) {
                                    m.setVerif("verifikasi");
                                } else {
                                    m.setVerif("Belum verif");
                                }
                                list.add(m);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PetaniPenjualan.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(PetaniPenjualan.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
