package com.example.brianr.agri;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class Penjual4Fragment extends Fragment {
    private Button penjualan;
    private String id;
    public Penjual4Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_penjual4, container, false);
        id = getArguments().getString("id");
        penjualan = (Button) view.findViewById(R.id.penjualan);
        penjualan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),PetaniPenjualan.class);
                i.putExtra("id",id);
                startActivity(i);
            }
        });
        return view;
    }

}
